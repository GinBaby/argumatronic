---
title: About
---

I have been a teacher, librarian, linguist, student of philosophy, and writer. I am still at least three of those things.

I cowrote [Haskell Programming from First Principles](http://haskellbook.com/).

I am now writing [The Joy of Haskell](https://joyofhaskell.com/).

I'm about half hippie chick and about half redneck, sympathetic to various political and economic positions along the anti-statist and voluntarist lines. I like loud music, kung fu movies, and hard liquor. I also like arts, crafts, stories, and the people who make them. I like gardening, canning, talking to my kids, and being in the mountains.

I'm interested in helping people learn to create technology, rather than simply consume it.

*"Don't take life so serious, son, it ain't nohow permanent."*

I am pretty serious about pie crust. Don't even come at me with that store bought stuff.
